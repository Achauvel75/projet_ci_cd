Afin de réaliser notre migration, nous avons réalisé un ensemble de fichier dans notre dépot gitlab :
- .gitlab-ci.yml
- docker compose 
- .env

la CI/CD réalise les étapes suivantes :

  - build
(l’étape de build va réaliser un build des images de notre registry gitlab afin de les push dans la VM)
 
  - preprod
(l’étape de test permet de se connecte à la preprod en ssh et de clone notre repository) 

  - loadtest-cloud
(ralise automatiquement un test de charge sur l’URL https://www.projetci.codelib.re/ ) 

  - cypress
(fichier login de test qui test la connexion à cypress) 

  - deploy
(Lorsque nous réalisons un commit cela lancera un serveur de test. 
Une fois l’ensemble des tests finalisés et validés, cela déploie les applications sur le serveur de prod et réalise la commande “docker compose up”.)

Accès Admin machine:
user : projetci
mdp : Projetci147!

Accès Azure:
master-2022.projet-pro-26@supdevinci-edu.fr  /   Caf18048!

Accès netlib : projetpro  /  Projetpro147!

Accès au dossier du TP : /home/projetpro/Desktop/projet_ci_cd

 NB: Il est possible que la CI ne fonctionne pas car nous avons été bannis pendant 3h par let's encrypt 
 IP du compte 20.56.38.109
 id: projetpro  /  mdp: Projetpro147!
 il faut changer l'ip du DNS par l'ip situé sur netlib.
