#!/bin/bash

# install docker
apt-get update
apt-get install docker

# import database in db container
docker ps
docker exec -i dwp_db-lets-jim mysql -uroot -proot -D DROP DATABASE wordpress
docker exec -i dwp_db-lets-jim mysql -uroot -proot -D wordpress < ./dump/wordpress.sql
